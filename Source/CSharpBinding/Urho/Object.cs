//
// Urho's Object C# sugar
//
// Authors:
//   Miguel de Icaza (miguel@xamarin.com)
//
// Copyrigh 2015 Xamarin INc
//

using System;
using System.Runtime.InteropServices;

namespace Urho {

	public partial class UrhoObject : RefCounted
	{
        protected UrhoObject(IntPtr handle) : base(handle)
        {

        }

    #if false
		[MonoPInvokeCallback(typeof(ObjectCallbackSignature))]
		internal static void ObjectCallback(IntPtr data, int stringHash, IntPtr variantMap)
		{
			GCHandle gch = GCHandle.FromIntPtr(data);
			Action<IntPtr> a = (Action<IntPtr>)gch.Target;
			a(variantMap);
		}
    #endif
	}
}
