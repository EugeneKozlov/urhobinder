﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Urho
{
    public partial class Engine
    {
        public static void ParseArguments(string[] args)
        {
            string[] padded = new string[args.Length + 1];
            args.CopyTo(padded, 1);
            binding_Engine_ParseArguments(padded, padded.Length);
        }

        [DllImport(Consts.NativeImport, CallingConvention = CallingConvention.Cdecl)]
        internal extern static void binding_Engine_ParseArguments(string[] args, int nargs);
    }
}
