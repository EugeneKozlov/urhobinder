﻿using System;
using System.Runtime.InteropServices;

namespace Urho
{
    internal static class Consts
    {
        public const string NativeImport = "NativeBinding.dll";
    }

#if false
    public partial class RefCounted : IDisposable
    {
        internal IntPtr handle_;

        public IntPtr Handle => handle_;

        protected RefCounted(IntPtr handle)
        {
            if (handle == IntPtr.Zero)
                throw new ArgumentException($"Attempted to instantiate a {GetType()} with a null handle");
            this.handle_ = handle;
            //Runtime.RegisterObject(this);
        }

        public void Dispose()
        {
            //Dispose(true);
            //GC.SuppressFinalize(this);
        }

    }
#endif

    public partial class RefCounted
    {
        public int Refs()
        {
            return ub_Urho_RefCounted_Refs(Handle);
        }

        [DllImport(Consts.NativeImport, CallingConvention = CallingConvention.Cdecl)]
        extern static int ub_Urho_RefCounted_Refs(IntPtr self);
    }

    public partial class Context : RefCounted
    {
        public Context() : base(ub_Urho_Context_Constructor())
        {
        }

        [DllImport(Consts.NativeImport, CallingConvention = CallingConvention.Cdecl)]
        private extern static IntPtr ub_Urho_Context_Constructor();

    }

    public partial class Application : UrhoObject
    {
        public Application(Context context) : base(ub_Urho_Application_Constructor(context.Handle))
        {
            ub_Urho_Application_Setup_set(Handle, (self) => Runtime.LookupObject<Application>(self)?.Setup());
            ub_Urho_Application_Start_set(Handle, (self) => ((Application)GCHandle.FromIntPtr(self).Target).Start());
            ub_Urho_Application_Stop_set(Handle, (self) => ((Application)GCHandle.FromIntPtr(self).Target).Stop());
        }

        public virtual void Setup() { }
        public virtual void Start() { }
        public virtual void Stop() { }

        public int Run()
        {
            return ub_Urho_Application_Run(Handle);
        }

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        delegate void SetupCallback(IntPtr self);

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        delegate void StartCallback(IntPtr self);

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        delegate void StopCallback(IntPtr self);

        [DllImport(Consts.NativeImport, CallingConvention = CallingConvention.Cdecl)]
        extern static IntPtr ub_Urho_Application_Constructor(IntPtr context);

        [DllImport(Consts.NativeImport, CallingConvention = CallingConvention.Cdecl)]
        extern static int ub_Urho_Application_Run(IntPtr self);

        [DllImport(Consts.NativeImport, CallingConvention = CallingConvention.Cdecl)]
        extern static IntPtr ub_Urho_Application_Setup_set(IntPtr self, [MarshalAs(UnmanagedType.FunctionPtr)]SetupCallback callback);

        [DllImport(Consts.NativeImport, CallingConvention = CallingConvention.Cdecl)]
        extern static IntPtr ub_Urho_Application_Start_set(IntPtr self, [MarshalAs(UnmanagedType.FunctionPtr)]StartCallback callback);

        [DllImport(Consts.NativeImport, CallingConvention = CallingConvention.Cdecl)]
        extern static IntPtr ub_Urho_Application_Stop_set(IntPtr self, [MarshalAs(UnmanagedType.FunctionPtr)]StopCallback callback);
    }
}
