﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;

namespace Sample
{
    class SampleApplication : Application
    {
        public SampleApplication(Context context) : base(context) { }
        public override void Setup()
        {
            Console.WriteLine("SampleApplication::Setup");
        }
        public override void Start()
        {
            Console.WriteLine("SampleApplication::Start");
        }
        public override void Stop()
        {
            Console.WriteLine("SampleApplication::Stop");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Engine.ParseArguments(args);
            Context context = new Context();
            SampleApplication application = new SampleApplication(context);
            application.Run();
        }
    }
}
