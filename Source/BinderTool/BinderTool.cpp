#include "../Binder/Binder.h"

int main(int argc, const char* argv[])
{
    PackageGenerationInfo info;
    info.headerFolder_  = "D:/Root/Repos/Urho3D/include";
    info.packageInfo_   = "D:/Root/Repos/UrhoBinder/Packages/Urho.xml";
    info.outputManaged_ = "D:/Root/Repos/UrhoBinder/Source/CSharpBinding/Urho.Generated.cs";
    info.outputNative_  = "D:/Root/Repos/UrhoBinder/Source/NativeBinding/Urho.Generated.cpp";
    GeneratePackage(info);
    return 0;
}
