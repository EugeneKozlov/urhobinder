#include "Binder.h"
#include "BinderStructs.h"
#include "NameVisitor.h"
#include "cplusplus/CPlusPlus.h"
#include "pugixml/pugixml.hpp"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <unordered_map>

using namespace CPlusPlus;
using namespace Binder;

PrimitiveTypeKind GetIntegerType(int kind)
{
    switch (kind)
    {
    case IntegerType::Char:
        return PrimitiveTypeKind::Char;
    case IntegerType::Char16:
        return PrimitiveTypeKind::Char16;
    case IntegerType::Char32:
        return PrimitiveTypeKind::Char32;
    case IntegerType::WideChar:
        return PrimitiveTypeKind::WideChar;
    case IntegerType::Bool:
        return PrimitiveTypeKind::Bool;
    case IntegerType::Short:
        return PrimitiveTypeKind::Short;
    case IntegerType::Int:
        return PrimitiveTypeKind::Int;
    case IntegerType::Long:
        return PrimitiveTypeKind::Long;
    case IntegerType::LongLong:
        return PrimitiveTypeKind::LongLong;
    default:
        return PrimitiveTypeKind::Unknown;
    }
}

PrimitiveTypeKind GetFloatType(int kind)
{
    switch (kind)
    {
    case FloatType::Float:
        return PrimitiveTypeKind::Float;
    case FloatType::Double:
    case FloatType::LongDouble:
        return PrimitiveTypeKind::Double;
    default:
        return PrimitiveTypeKind::Unknown;
    }
}

FacadeFunction LoadFacadeFunction(pugi::xml_node node, bool isVirtual)
{
    FacadeFunction facadeFunction;
    facadeFunction.name_ = node.attribute("name").as_string();
    facadeFunction.nativeName_ = node.attribute("native").as_string();
    if (facadeFunction.nativeName_.empty())
        facadeFunction.nativeName_ = facadeFunction.name_;
    facadeFunction.overload_ = node.attribute("overload").as_int();
    facadeFunction.virtual_ = isVirtual;
    return facadeFunction;
}

void LoadPackageConfig(const std::string& fileName, PackageData& out)
{
    // Load XML document
    pugi::xml_document doc;
    doc.load_file(fileName.c_str());
    auto rootNode = doc.child("package");

    // Load attributes
    out.name_ = rootNode.attribute("name").as_string();
    out.namespace_ = rootNode.attribute("namespace").as_string();

    // Read headers
    for (auto headerNode = rootNode.child("header"); headerNode; headerNode = headerNode.next_sibling("header"))
        out.headers_.push_back(headerNode.attribute("name").as_string());

    // Read classes
    for (auto classNode = rootNode.child("class"); classNode; classNode = classNode.next_sibling("class"))
    {
        FacadeClass facadeClass;
        facadeClass.name_ = classNode.attribute("name").as_string();
        facadeClass.nativeName_ = classNode.attribute("native").as_string();
        if (facadeClass.nativeName_.empty())
            facadeClass.nativeName_ = facadeClass.name_;
        for (auto baseNode = classNode.child("base"); baseNode; baseNode = baseNode.next_sibling("base"))
            facadeClass.baseClasses_.push_back(baseNode.attribute("name").as_string());
        for (auto functionNode = classNode.child("function"); functionNode; functionNode = functionNode.next_sibling("function"))
            facadeClass.memberFunctions_.push_back(LoadFacadeFunction(functionNode, false));
        for (auto virtualNode = classNode.child("virtual"); virtualNode; virtualNode = virtualNode.next_sibling("virtual"))
            facadeClass.memberFunctions_.push_back(LoadFacadeFunction(virtualNode, true));
        out.facade_.classes_.push_back(std::move(facadeClass));
    }
}

void LoadNativeType(FullySpecifiedType typeName, NativeType& out)
{
    Type* type = typeName.type();
    Type* nestedType = type;
    if (type->isPointerType())
    {
        out.pointer_ = true;
        nestedType = type->asPointerType()->elementType().type();
    }
    else if (type->isReferenceType())
    {
        out.reference_ = true;
        nestedType = type->asReferenceType()->elementType().type();
    }

    if (nestedType->isIntegerType())
    {
        IntegerType* integerType = nestedType->asIntegerType();
        out.primitive_ = GetIntegerType(integerType->kind());
    }
    else if (nestedType->isFloatType())
    {
        FloatType* floatType = nestedType->asFloatType();
        out.primitive_ = GetFloatType(floatType->kind());
    }
    else if (nestedType->isNamedType())
    {
        out.name_ = GetSymbolName(nestedType->asNamedType()->name());
    }
}

void LoadNativeFunction(Function* pfunction, Class* pclass, NativeFunction& out)
{
    out.name_ = GetSymbolName(pfunction->name());
    out.const_ = pfunction->isConst();
    out.constructor_ = out.name_ == GetSymbolName(pclass->name());
    out.destructor_ = !out.name_.empty() && out.name_[0] == '~';
    if (pfunction->hasReturnType())
    {
        LoadNativeType(pfunction->returnType(), out.returnType_);
    }
    for (unsigned i = 0; i < pfunction->argumentCount(); ++i)
    {
        if (Argument* argument = pfunction->argumentAt(i)->asArgument())
        {
            NativeTypeName typeName;
            typeName.name_ = GetSymbolName(argument->name());
            LoadNativeType(argument->type(), typeName.type_);
            out.arguments_.push_back(typeName);
        }
    }
    if (out.constructor_)
    {
        out.returnType_.pointer_ = true;
        out.returnType_.name_ = "RefCounted";
    }
}

void LoadNativeClass(Class* pclass, NativeClass& out)
{
    out.name_ = GetSymbolName(pclass->name());
    for (unsigned i = 0; i < pclass->memberCount(); ++i)
    {
        Symbol* psymbol = pclass->memberAt(i);
        Function* pfunction = psymbol->asFunction();
        Declaration* pdecl = psymbol->asDeclaration();
        if (!pfunction && pdecl)
            pfunction = pdecl->type()->asFunctionType();

        if (pfunction && pfunction->isPublic())
        {
            NativeFunction nativeFunction;
            LoadNativeFunction(pfunction, pclass, nativeFunction);
            out.memberFunctions_.push_back(std::move(nativeFunction));
        }
    }
    for (unsigned i = 0; i < pclass->baseClassCount(); ++i)
    {
        out.baseClasses_.push_back(GetSymbolName(pclass->baseClassAt(i)->name()));
    }
}

class NamespaceVisitor : public SymbolVisitor
{
public:
    NamespaceVisitor(NativePackage& package, Namespace& globalNamespace)
        : package_(package)
        , globalNamespace_(globalNamespace)
    {
        accept(&globalNamespace_);
    }

    virtual bool visit(Template *t)
    {
        return false;
    }
    virtual bool visit(Enum *penum)
    {
#if 0
        // don't want enum's in classes
        if (classes_.Size())
            return true;

        JSBModule* module = header_->GetModule();

        JSBEnum* jenum = new JSBEnum(header_->GetContext(), module_, getNameString(penum->name()));
        jenum->SetHeader(header_);

        for (unsigned i = 0; i < penum->memberCount(); i++)
        {
            Symbol* symbol = penum->memberAt(i);

            String name = getNameString(symbol->name());
            String value = "";

            Declaration* decl = symbol->asDeclaration();
            if (decl)
            {
                EnumeratorDeclaration* enumDecl = decl->asEnumeratorDeclarator();
                const StringLiteral* constantValue = enumDecl->constantValue();
                if (constantValue)
                {
                    value = constantValue->chars();
                }
            }

            jenum->AddValue(name, value);

        }

        jenum->Preprocess();

        module->RegisterEnum(jenum);

#endif // 0
        return true;
    }

    virtual bool visit(Class *pclass)
    {
        NativeClass nativeClass;
        LoadNativeClass(pclass, nativeClass);
        package_.classes_.push_back(std::move(nativeClass));

#if 0
        classes_.Push(klass);

        String name = getNameString(klass->name());

        JSBModule* module = header_->GetModule();

        module->RegisterClass(name);
#endif // 0

        return true;
    }

    void postVisit(Symbol* symbol)
    {
        if (symbol->asClass())
        {
            //classes_.Remove((Class*)symbol);
        }
    }

private:
    NativePackage& package_;
    Namespace& globalNamespace_;
};

void LoadPackageHeader(const std::string& fileName, PackageData& out)
{
    // TODO: refactor this piece of shit
    Control control;
    const StringLiteral *fileId = control.stringLiteral(fileName.c_str(), fileName.length());
    LanguageFeatures features;
    features.qtEnabled = false;
    features.qtMocRunEnabled = false;
    features.qtKeywordsEnabled = false;
    features.cxx11Enabled = true;
    features.objCEnabled = false;
    features.c99Enabled = false;
    TranslationUnit* translationUnit = new TranslationUnit(&control, fileId);
    translationUnit->setLanguageFeatures(features);
    control.switchTranslationUnit(translationUnit);

    std::ifstream file(fileName);
    if (!file)
    {
        std::cerr << "File <" << fileName << "> is not found!\n";
        return;
    }

    std::stringstream buffer;
    buffer << file.rdbuf();
    std::string fileData = buffer.str();

    translationUnit->blockErrors(true);

    translationUnit->setSource(fileData.data(), fileData.length());
    translationUnit->tokenize();
    translationUnit->parse();

    Namespace* globalNamespace = control.newNamespace(0);
    Bind semantic(translationUnit);
    semantic.setSkipFunctionBodies(true);

    semantic(translationUnit->ast()->asTranslationUnit(), globalNamespace);

    // Get current namespace
    Namespace* currentNamespace = nullptr;
    if (out.namespace_.empty())
        currentNamespace = globalNamespace;
    else
    {
        for (unsigned i = 0; i < globalNamespace->memberCount(); ++i)
        {
            Symbol* xsymbol = globalNamespace->memberAt(i);
            if (Namespace* xnamespace = xsymbol->asNamespace())
                if (GetSymbolName(xnamespace->name()) == out.namespace_)
                {
                    currentNamespace = xnamespace;
                    break;
                }
        }
    }
    if (!currentNamespace)
    {
        std::cerr << "Cannot find base namespace! module=" << out.name_ << " namespace=" << out.namespace_ << std::endl;
        return;
    }

    NamespaceVisitor(out.native_, *currentNamespace);
}

template <class T, std::string T::*Name = &T::name_>
void GenerateLookup(std::vector<T>& array, std::unordered_map<std::string, T*>& map)
{
    for (T& elem : array)
        map[elem.*Name] = &elem;
}

void PostprocessPackage(PackageData& package)
{
    // Generate lookup
    GenerateLookup(package.native_.classes_, package.native_.classesLookup_);
    for (NativeClass& nativeClass : package.native_.classes_)
        GenerateLookup(nativeClass.memberFunctions_, nativeClass.memberFunctionsLookup_);

    GenerateLookup(package.facade_.classes_, package.facade_.classesLookup_);
    for (FacadeClass& facadeClass : package.facade_.classes_)
        GenerateLookup(facadeClass.memberFunctions_, facadeClass.memberFunctionsLookup_);

    // Generate mapping
    for (const FacadeClass& facadeClass : package.facade_.classes_)
        package.nativeToManaged_[facadeClass.nativeName_] = facadeClass.name_;
}

void GenerateCSharpBindings(const PackageData& package, const std::string& outputNative, const std::string& outputManaged)
{
    std::ofstream nativeStream(outputNative);
    std::ofstream managedStream(outputManaged);

    // Print native header
    nativeStream
        << "#ifdef URHO3D_PLATFORM_WINDOWS\n"
        << "#define URHO3D_EXPORT_API __declspec(dllexport)\n"
        << "#else\n"
        << "#define URHO3D_EXPORT_API\n"
        << "#endif\n\n";

    // Print managed header
    managedStream
        << "using System;\n"
        << "using System.Runtime.InteropServices;\n\n";

    // Print native includes
    for (const std::string& header : package.headers_)
        nativeStream << "#include <" << header << ">\n";

    // Print native usings
    nativeStream << "\nusing namespace " << package.namespace_ << ";\n";

    // Prepare wrappers
    for (const FacadeClass& facadeClass : package.facade_.classes_)
    {
        auto iterBegin = facadeClass.memberFunctions_.begin();
        auto iterEnd = facadeClass.memberFunctions_.end();
        auto isVirtual = [](const FacadeFunction& facadeFunction) { return facadeFunction.virtual_; };
        if (std::find_if(iterBegin, iterEnd, isVirtual) == iterEnd)
            continue;

        NativeClass* nativeClass = package.native_.FindClass(facadeClass.nativeName_);
        if (!nativeClass)
            continue;

        nativeClass->wrapperName_ = "ub_wrapper_" + package.name_ + "_" + nativeClass->name_;
        for (const FacadeFunction& facadeFunction : facadeClass.memberFunctions_)
        {
            NativeFunction* nativeFunction = nativeClass->FindFunction(facadeFunction.nativeName_);
            if (!nativeFunction || !facadeFunction.virtual_)
                continue;

            const std::string fullName = nativeFunction->name_ + "_" + std::to_string(facadeFunction.overload_);
            nativeFunction->gen_.callbackTypeName_ = "ub_callback_" + fullName;
            nativeFunction->gen_.callbackType_ = GenerateCFunctionDeclNative(nativeClass->name_, "(__stdcall *)", *nativeFunction);
            nativeFunction->gen_.callbackName_ = "ub_impl_" + fullName;
            nativeFunction->gen_.callbackDefaultName_ = "ub_default_" + fullName;
            nativeFunction->gen_.callbackSetter_ = "ub_setter_" + nativeClass->name_ + "_" + fullName;
        }
    }

    // Print native wrappers
    for (const FacadeClass& facadeClass : package.facade_.classes_)
    {

        NativeClass* nativeClass = package.native_.FindClass(facadeClass.nativeName_);
        if (!nativeClass || nativeClass->wrapperName_.empty())
            continue;

        nativeStream << "\nclass " << nativeClass->wrapperName_ << " : public " << nativeClass->name_ << "\n{\npublic:\n";
        nativeStream << "\tusing " << nativeClass->name_ << "::" << nativeClass->name_ << ";\n";

        for (const NativeFunction& nativeFunction : nativeClass->memberFunctions_)
        {
            if (nativeFunction.gen_.callbackName_.empty())
                continue;

            nativeStream << "\n\tusing " << nativeFunction.gen_.callbackTypeName_
                << " = " << nativeFunction.gen_.callbackType_ << ";\n";
            nativeStream << "\t" << nativeFunction.gen_.callbackTypeName_ << " " << nativeFunction.gen_.callbackName_ << ";\n";
            nativeStream << "\tvirtual void " << nativeFunction.name_ << "() override { " << nativeFunction.gen_.callbackName_ << "(this); }\n";
            nativeStream << "\tvoid " << nativeFunction.gen_.callbackDefaultName_ << "() { " << nativeClass->name_ << "::" << nativeFunction.name_ << "();}\n";
        }

        nativeStream << "};\n";
    }

    // Print native bindings begin guard
    nativeStream << "\nextern \"C\"\n{\n\n";
    managedStream << "namespace " << package.name_ << "\n{\n";

    // Print class bindings
    int idx = 1;
    for (const FacadeClass& facadeClass : package.facade_.classes_)
    {
        NativeClass* nativeClass = package.native_.FindClass(facadeClass.nativeName_);
        if (!nativeClass)
            continue;

        // Print managed class binding
        managedStream << "\tpublic partial class " << facadeClass.name_;
        if (!facadeClass.baseClasses_.empty())
        {
            managedStream << " : ";
            bool first = true;
            for (const std::string& baseClass : facadeClass.baseClasses_)
            {
                if (!first)
                    managedStream << ", ";
                first = false;
                managedStream << baseClass;
            }
        }
        managedStream << "\n\t{\n";

        // Print function bindings
        for (const FacadeFunction& facadeFunction : facadeClass.memberFunctions_)
        {
            NativeFunction* nativeFunction = nativeClass->FindFunction(facadeFunction.nativeName_);
            if (!nativeFunction)
                continue;

            const std::string newTypeName = nativeClass->wrapperName_.empty() ? nativeClass->name_ : nativeClass->wrapperName_;
            const std::string bindingName =
                "bind_" + package.name_ + "_" + nativeClass->name_ + "_" + nativeFunction->name_ + "_" + std::to_string(idx++);
            const std::string managedCDecl = GenerateCFunctionDeclCSharp(nativeClass->name_, bindingName, *nativeFunction);
            const std::string managedDecl = GenerateManagedFunctionDeclCSharp(facadeClass.name_, facadeFunction.name_, *nativeFunction, package.nativeToManaged_);
            const std::string managedCall = GenerateManagedFunctionCallCSharp(facadeClass.name_, bindingName, *nativeFunction, package.nativeToManaged_);

            // Print managed implementation
            managedStream << "\t\t[DllImport(Consts.NativeImport, CallingConvention = CallingConvention.Cdecl)]\n";
            managedStream << "\t\tprivate extern static " << managedCDecl << ";\n\n";

            if (facadeFunction.virtual_)
            {
                NativeFunction defaultFunction = *nativeFunction;
                defaultFunction.name_ = nativeFunction->gen_.callbackDefaultName_;

                // Print native function
                nativeStream << "URHO3D_EXPORT_API " << GenerateCFunctionDeclNative(nativeClass->wrapperName_, bindingName, *nativeFunction) << "\n{\n";
                nativeStream << "\t" << GenerateCxxFunctionCallNative(newTypeName, bindingName, defaultFunction) << ";\n}\n";

                // Print native callback setter
                nativeStream << "URHO3D_EXPORT_API void " << nativeFunction->gen_.callbackSetter_
                    << "(" << nativeClass->wrapperName_ << "* self, "
                    << nativeClass->wrapperName_ << "::" << nativeFunction->gen_.callbackTypeName_ << " callback)" << "\n{\n";
                nativeStream << "\tself->" << nativeFunction->gen_.callbackName_ << " = callback;\n}\n";

                // Print managed facade
                managedStream << "\t\t[UnmanagedFunctionPointer(CallingConvention.StdCall)]\n";
                managedStream << "\t\tdelegate void " << nativeFunction->gen_.callbackTypeName_ << "(IntPtr self);\n\n";

                managedStream << "\t\t[DllImport(Consts.NativeImport, CallingConvention = CallingConvention.Cdecl)]\n";
                managedStream << "\t\tprivate extern static void " << nativeFunction->gen_.callbackSetter_ << "(IntPtr self, "
                    << "[MarshalAs(UnmanagedType.FunctionPtr)]" << nativeFunction->gen_.callbackTypeName_ << " callback);\n\n";

                managedStream << "\t\tpublic virtual " << managedDecl << "\n\t\t{\n\t\t\t" << managedCall << ";\n\t\t}\n\n";
            }
            else
            {
                // Print native function
                nativeStream << "URHO3D_EXPORT_API " << GenerateCFunctionDeclNative(nativeClass->name_, bindingName, *nativeFunction) << "\n{\n";
                nativeStream << "\t" << GenerateCxxFunctionCallNative(newTypeName, bindingName, *nativeFunction) << ";\n}\n";

                // Print managed facade
                if (nativeFunction->constructor_)
                {
                    managedStream << "\t\tpublic " << managedDecl << " : base(" << managedCall << ")\n\t\t{\n";

                    // Setup virtual functions
                    for (const FacadeFunction& facadeFunction : facadeClass.memberFunctions_)
                    {
                        NativeFunction* nativeFunction = nativeClass->FindFunction(facadeFunction.nativeName_);
                        if (!nativeFunction || !facadeFunction.virtual_)
                            continue;

                        managedStream << "\t\t\t" << nativeFunction->gen_.callbackSetter_
                            << "(Handle, (self) => Runtime.LookupObject<" << facadeClass.name_ << ">(self)?." << facadeFunction.name_ << "());\n";
                    }

                    managedStream << "\t\t}\n\n";
                }
                else
                {
                    managedStream << "\t\tpublic " << managedDecl << "\n\t\t{\n\t\t\t" << managedCall << ";\n\t\t}\n\n";
                }
            }
        }

        managedStream << "\t}\n\n";
    }

    // Print native bindings end guard
    nativeStream << "\n}\n";
    managedStream << "}\n";
}

void GeneratePackage(const PackageGenerationInfo& info)
{
    PackageData packageData;
    LoadPackageConfig(info.packageInfo_, packageData);
    for (const auto& header : packageData.headers_)
        LoadPackageHeader(info.headerFolder_ + "/" + header, packageData);
    PostprocessPackage(packageData);
    GenerateCSharpBindings(packageData, info.outputNative_, info.outputManaged_);
}

