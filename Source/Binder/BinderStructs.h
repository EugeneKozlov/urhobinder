#include <vector>
#include <string>
#include <unordered_map>

namespace Binder
{

enum class PrimitiveTypeKind
{
    Unknown,
    Char,
    Char16,
    Char32,
    WideChar,
    Bool,
    Short,
    Int,
    Long,
    LongLong,
    Float,
    Double
};

struct NativeType
{
    std::string name_;
    PrimitiveTypeKind primitive_ = PrimitiveTypeKind::Unknown;
    bool const_ = false;
    bool pointer_ = false;
    bool reference_ = false;
    bool marshalled_ = false;
};

inline bool IsVoid(const NativeType& type)
{
    return type.name_.empty() && type.primitive_ == PrimitiveTypeKind::Unknown;
}

inline bool IsRefCounted(const NativeType& type)
{
    return !type.name_.empty() && !type.marshalled_;
}

using StringMapping = std::unordered_map<std::string, std::string>;

struct NativeTypeName
{
    std::string name_;
    NativeType type_;
};

struct NativeVirtualFunction
{
    std::string callbackType_;
    std::string callbackTypeName_;
    std::string callbackName_;
    std::string callbackDefaultName_;
    std::string callbackSetter_;
};

struct NativeFunction
{
    std::string name_;
    NativeType returnType_;
    std::vector<NativeTypeName> arguments_;
    bool const_ = false;
    bool constructor_ = false;
    bool destructor_ = false;
    NativeVirtualFunction gen_;
};

struct NativeClass
{
    std::string name_;
    std::vector<std::string> baseClasses_;
    std::vector<NativeFunction> memberFunctions_;
    std::unordered_map<std::string, NativeFunction*> memberFunctionsLookup_;
    NativeFunction* FindFunction(const std::string& name) const
    {
        auto iter = memberFunctionsLookup_.find(name);
        return iter != memberFunctionsLookup_.end() ? iter->second : nullptr;
    }
    std::string wrapperName_;
};

struct NativePackage
{
    std::vector<NativeClass> classes_;
    std::unordered_map<std::string, NativeClass*> classesLookup_;
    NativeClass* FindClass(const std::string& name) const
    {
        auto iter = classesLookup_.find(name);
        return iter != classesLookup_.end() ? iter->second : nullptr;
    }
};

struct FacadeFunction
{
    std::string name_;
    std::string nativeName_;
    int overload_ = 0;
    bool virtual_ = false;
};

struct FacadeClass
{
    std::string name_;
    std::string nativeName_;
    std::vector<std::string> baseClasses_;
    std::vector<FacadeFunction> memberFunctions_;
    std::unordered_map<std::string, FacadeFunction*> memberFunctionsLookup_;
};

struct FacadeDescription
{
    std::vector<FacadeClass> classes_;
    std::unordered_map<std::string, FacadeClass*> classesLookup_;
};

struct PackageData
{
    std::string name_;
    std::string namespace_;
    std::vector<std::string> headers_;
    NativePackage native_;
    FacadeDescription facade_;
    StringMapping nativeToManaged_;
};

inline std::string LookupMapping(const StringMapping& mapping, const std::string& key)
{
    auto iter = mapping.find(key);
    return iter != mapping.end() ? iter->second : key;
}

std::string GetPrimitiveTypeCName(PrimitiveTypeKind primitive);

std::string GetPrimitiveTypeCSharpName(PrimitiveTypeKind primitive);

std::string GetTypeNameC(NativeType type);

std::string GetTypeNameCSharp(NativeType type, const StringMapping& mapping, bool addConst = true);

std::string GetBindingTypeNameCSharp(NativeType type);

std::string GetTypeTransitionNativeToC(NativeTypeName type);

std::string GenerateCFunctionDeclNative(const std::string& className, const std::string& functionName, const NativeFunction& desc);

std::string GenerateCxxFunctionCallNative(const std::string& className, const std::string& functionName, const NativeFunction& desc);

std::string GenerateCFunctionDeclCSharp(const std::string& className, const std::string& functionName, const NativeFunction& desc);

std::string GenerateManagedFunctionDeclCSharp(const std::string& className, const std::string& functionName, const NativeFunction& desc,
    const StringMapping& mapping);

std::string GenerateManagedFunctionCallCSharp(const std::string& className, const std::string& functionName, const NativeFunction& desc,
    const StringMapping& mapping);

}
