#pragma once

#include "cplusplus/CPlusPlus.h"
#include <string>

class NameVisitorImpl : protected CPlusPlus::NameVisitor
{
public:
    NameVisitorImpl() { }
    virtual ~NameVisitorImpl() { }

    std::string operator()(const CPlusPlus::Name* name);

private:
    virtual void visit(const CPlusPlus::Identifier* name);
    virtual void visit(const CPlusPlus::TemplateNameId* name);
    virtual void visit(const CPlusPlus::DestructorNameId* name);
    virtual void visit(const CPlusPlus::OperatorNameId* name);
    virtual void visit(const CPlusPlus::ConversionNameId* name);
    virtual void visit(const CPlusPlus::QualifiedNameId* name);
    virtual void visit(const CPlusPlus::SelectorNameId* name);
    virtual void visit(const CPlusPlus::AnonymousNameId* name);

    std::string name_;
};

inline std::string GetSymbolName(const CPlusPlus::Name* name)
{
    return NameVisitorImpl()(name);
}
