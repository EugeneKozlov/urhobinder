#pragma once

#include <string>

struct PackageGenerationInfo
{
    std::string headerFolder_;
    std::string packageInfo_;
    std::string outputNative_;
    std::string outputManaged_;
};

void GeneratePackage(const PackageGenerationInfo& info);
