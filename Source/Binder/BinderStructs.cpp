#include "BinderStructs.h"

using namespace std;

namespace Binder
{

std::string GetPrimitiveTypeCName(PrimitiveTypeKind primitive)
{
    switch (primitive)
    {
    case PrimitiveTypeKind::Char: return "char";
    case PrimitiveTypeKind::Bool: return "char";
    case PrimitiveTypeKind::Short: return "short";
    case PrimitiveTypeKind::Int: return "int";
    case PrimitiveTypeKind::Long: return "int";
    case PrimitiveTypeKind::LongLong: return "long long";
    case PrimitiveTypeKind::Float: return "float";
    case PrimitiveTypeKind::Double: return "double";
    default: return "unknown";
    }
}

std::string GetPrimitiveTypeCSharpName(PrimitiveTypeKind primitive)
{
    switch (primitive)
    {
    case PrimitiveTypeKind::Char: return "char";
    case PrimitiveTypeKind::Bool: return "bool";
    case PrimitiveTypeKind::Short: return "short";
    case PrimitiveTypeKind::Int: return "int";
    case PrimitiveTypeKind::Long: return "int";
    case PrimitiveTypeKind::LongLong: return "long";
    case PrimitiveTypeKind::Float: return "float";
    case PrimitiveTypeKind::Double: return "double";
    default: return "unknown";
    }
}

std::string GetTypeNameC(NativeType type)
{
    if (IsVoid(type))
        return "void";

    std::string name;
    if (type.const_)
        name += "const ";

    name += type.name_.empty() ? GetPrimitiveTypeCName(type.primitive_) : type.name_;

    if (type.pointer_ || type.reference_)
        name += "*";

    return name;
}

std::string GetTypeNameCSharp(NativeType type, const StringMapping& mapping, bool addConst /*= true*/)
{
    std::string result;
    if (type.const_ && addConst)
        result += "const ";

    if (IsVoid(type))
        result += "void";
    else if (type.name_.empty())
        result += GetPrimitiveTypeCSharpName(type.primitive_);
    else
        result += LookupMapping(mapping, type.name_);

    return result;
}

std::string GetBindingTypeNameCSharp(NativeType type)
{
    if (IsVoid(type))
        return "void";
    else if (type.name_.empty())
        return GetPrimitiveTypeCSharpName(type.primitive_);
    else
        return "IntPtr";
}

std::string GetTypeTransitionNativeToC(NativeTypeName type)
{
    return "";
}

std::string GenerateCFunctionDeclNative(const std::string& className, const std::string& functionName, const NativeFunction& desc)
{
    string result;
    result += GetTypeNameC(desc.returnType_);
    result += " ";
    result += functionName;
    result += "(";
    bool first = true;
    if (!className.empty() && !desc.constructor_)
    {
        first = false;
        if (desc.const_)
            result += "const ";
        result += className;
        result += "* self";
    }
    for (const NativeTypeName& arg : desc.arguments_)
    {
        if (!first)
            result += ", ";
        first = false;
        result += GetTypeNameC(arg.type_);
        result += " ";
        result += arg.name_;
    }
    result += ")";
    return result;
}

std::string GenerateCxxFunctionCallNative(const std::string& className, const std::string& functionName, const NativeFunction& desc)
{
    string result;
    if (!IsVoid(desc.returnType_))
        result += "return ";
    if (desc.constructor_)
    {
        result += "new ";
        result += className;
    }
    else
    {
        if (desc.returnType_.reference_)
            result += "&";
        result += "self->";
        result += desc.name_;
    }
    result += "(";
    bool first = true;
    for (const NativeTypeName& arg : desc.arguments_)
    {
        if (!first)
            result += ", ";
        first = false;
        if (arg.type_.reference_)
            result += "*";
        result += arg.name_;
    }
    result += ")";
    return result;
}

std::string GenerateCFunctionDeclCSharp(const std::string& className, const std::string& functionName, const NativeFunction& desc)
{
    string result;
    result += GetBindingTypeNameCSharp(desc.returnType_);
    result += " ";
    result += functionName;
    result += "(";
    bool first = true;
    if (!className.empty() && !desc.constructor_)
    {
        first = false;
        result += "IntPtr self";
    }
    for (const NativeTypeName& arg : desc.arguments_)
    {
        if (!first)
            result += ", ";
        first = false;
        result += GetBindingTypeNameCSharp(arg.type_);
        result += " ";
        result += arg.name_;
    }
    result += ")";
    return result;
}

std::string GenerateManagedFunctionDeclCSharp(const std::string& className, const std::string& /*functionName*/, const NativeFunction& desc, const StringMapping& mapping)
{
    string result;
    if (!desc.constructor_)
    {
        result += GetTypeNameCSharp(desc.returnType_, mapping);
        result += " ";
        result += desc.name_;
    }
    else
        result += LookupMapping(mapping, className);
    result += "(";
    bool first = true;
    for (const NativeTypeName& arg : desc.arguments_)
    {
        if (!first)
            result += ", ";
        first = false;
        result += GetTypeNameCSharp(arg.type_, mapping);
        result += " ";
        result += arg.name_;
    }
    result += ")";
    return result;
}

std::string GenerateManagedFunctionCallCSharp(const std::string& className, const std::string& functionName, const NativeFunction& desc, const StringMapping& mapping)
{
    string result;
    if (!IsVoid(desc.returnType_) && !desc.constructor_)
        result += "return ";
    if (IsRefCounted(desc.returnType_) && !desc.constructor_)
    {
        result += "Runtime.LookupObject<";
        result += GetTypeNameCSharp(desc.returnType_, mapping, false);
        result += ">(";
    }
    result += functionName;
    result += "(";
    bool first = true;
    if (!desc.constructor_)
    {
        first = false;
        result += "Handle";
    }
    for (const NativeTypeName& arg : desc.arguments_)
    {
        if (!first)
            result += ", ";
        first = false;
        result += arg.name_;
        if (IsRefCounted(arg.type_))
            result += ".Handle";
    }
    result += ")";
    if (IsRefCounted(desc.returnType_) && !desc.constructor_)
        result += ")";
    return result;
}

}
