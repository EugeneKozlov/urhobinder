#include "../Catch/catch.hpp"
#include "../Binder/BinderStructs.h"

namespace Binder
{

using namespace std;

namespace Tests
{

NativeType MakeVoid() { return NativeType{}; }

NativeType MakePrimitiveValue(PrimitiveTypeKind type)
{
    NativeType result;
    result.primitive_ = type;
    return result;
}

NativeType MakeRefCountedPointer(string type, bool isConst)
{
    NativeType result;
    result.name_ = type;
    result.const_ = isConst;
    result.pointer_ = true;
    return result;
}

NativeType MakeRefCountedReference(string type, bool isConst)
{
    NativeType result;
    result.name_ = type;
    result.const_ = isConst;
    result.reference_ = true;
    return result;
}

NativeFunction MakeFunction(string name, NativeType returnType, std::vector<NativeTypeName> arguments, bool isConst)
{
    NativeFunction result;
    result.name_ = name;
    result.returnType_ = returnType;
    result.arguments_ = arguments;
    result.const_ = isConst;
    return result;
}

NativeFunction MakeConstructor(string name, string className, string returnType, std::vector<NativeTypeName> arguments)
{
    NativeFunction result;
    result.constructor_ = true;
    result.name_ = name;
    result.returnType_.name_ = returnType;
    result.returnType_.pointer_ = true;
    result.arguments_ = arguments;
    return result;
}

// NativeType MakePrimitivePointer(PrimitiveTypeKind type) { return{ "", type, false, true, false, false }; }
// NativeType MakePrimitiveReference(PrimitiveTypeKind type) { return{ "", type, false, false, true, false }; }
// NativeType MakePrimitiveConstPointer(PrimitiveTypeKind type) { return{ "", type, true, true, false, false }; }
// NativeType MakePrimitiveConstReference(PrimitiveTypeKind type) { return{ "", type, true, false, true, false }; }

// NativeType MakeMarshalledValue(const string type) { return{ type, PrimitiveTypeKind::Unknown, false, false, false, true }; }
// NativeType MakeMarshalledPointer(const string type) { return{ type, PrimitiveTypeKind::Unknown, false, true, false, true }; }
// NativeType MakeMarshalledReference(const string type) { return{ type, PrimitiveTypeKind::Unknown, false, false, true, true }; }
// NativeType MakeMarshalledConstPointer(const string type) { return{ type, PrimitiveTypeKind::Unknown, true, true, false, true }; }
// NativeType MakeMarshalledConstReference(const string type) { return{ type, PrimitiveTypeKind::Unknown, true, false, true, true }; }

// NativeType MakeRefCountedPointer(const string type) { return{ type, PrimitiveTypeKind::Unknown, false, true, false, true }; }
// NativeType MakeRefCountedReference(const string type) { return{ type, PrimitiveTypeKind::Unknown, false, false, true, true }; }
// NativeType MakeRefCountedConstPointer(const string type) { return{ type, PrimitiveTypeKind::Unknown, true, true, false, true }; }
// NativeType MakeRefCountedConstReference(const string type) { return{ type, PrimitiveTypeKind::Unknown, true, false, true, true }; }

static const StringMapping emptyMapping;

TEST_CASE("TestConstructorMarshaling")
{
    {
        auto f = MakeConstructor("f", "Object", "RefCounted", {});

        REQUIRE("RefCounted* f()" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("return new Object()" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("IntPtr f()" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("Object()" == GenerateManagedFunctionDeclCSharp("Object", "f", f, emptyMapping));
        REQUIRE("f()" == GenerateManagedFunctionCallCSharp("Object", "f", f, emptyMapping));
    }
    {
        auto f = MakeConstructor("f", "Object", "RefCounted", {
            { "param1", MakePrimitiveValue(PrimitiveTypeKind::Int) },
            { "param2", MakePrimitiveValue(PrimitiveTypeKind::Double) },
        });

        REQUIRE("RefCounted* f(int param1, double param2)" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("return new Object(param1, param2)" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("IntPtr f(int param1, double param2)" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("Object(int param1, double param2)" == GenerateManagedFunctionDeclCSharp("Object", "f", f, emptyMapping));
        REQUIRE("f(param1, param2)" == GenerateManagedFunctionCallCSharp("Object", "f", f, emptyMapping));
    }
}

TEST_CASE("TestSelfMarshaling")
{
    {
        auto f = MakeFunction("f", MakeVoid(), {}, false);

        REQUIRE("void f(Object* self)" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("self->f()" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("void f(IntPtr self)" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("void f()" == GenerateManagedFunctionDeclCSharp("Object", "f", f, emptyMapping));
        REQUIRE("f(Handle)" == GenerateManagedFunctionCallCSharp("Object", "f", f, emptyMapping));
    }
    {
        auto f = MakeFunction("f", MakeVoid(), {}, true);

        REQUIRE("void f(const Object* self)" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("self->f()" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("void f(IntPtr self)" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("void f()" == GenerateManagedFunctionDeclCSharp("Object", "f", f, emptyMapping));
        REQUIRE("f(Handle)" == GenerateManagedFunctionCallCSharp("Object", "f", f, emptyMapping));
    }

}

TEST_CASE("TestPrimitiveArgumentsParametersMarshaling")
{
    {
        auto f = MakeFunction("f", MakeVoid(), {
            { "param1", MakePrimitiveValue(PrimitiveTypeKind::Int) },
            { "param2", MakePrimitiveValue(PrimitiveTypeKind::Double) },
        }, false);

        REQUIRE("void f(Object* self, int param1, double param2)" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("self->f(param1, param2)" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("void f(IntPtr self, int param1, double param2)" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("void f(int param1, double param2)" == GenerateManagedFunctionDeclCSharp("Object", "f", f, emptyMapping));
        REQUIRE("f(Handle, param1, param2)" == GenerateManagedFunctionCallCSharp("Object", "f", f, emptyMapping));
    }
}

TEST_CASE("TestPrimitiveReturnMarshaling")
{
    {
        auto f = MakeFunction("f", MakeVoid(), {}, false);

        REQUIRE("void f(Object* self)" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("self->f()" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("void f(IntPtr self)" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("void f()" == GenerateManagedFunctionDeclCSharp("Object", "f", f, emptyMapping));
        REQUIRE("f(Handle)" == GenerateManagedFunctionCallCSharp("Object", "f", f, emptyMapping));
    }

    {
        auto f = MakeFunction("f", MakePrimitiveValue(PrimitiveTypeKind::Int), {}, false);

        REQUIRE("int f(Object* self)" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("return self->f()" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("int f(IntPtr self)" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("int f()" == GenerateManagedFunctionDeclCSharp("Object", "f", f, emptyMapping));
        REQUIRE("return f(Handle)" == GenerateManagedFunctionCallCSharp("Object", "f", f, emptyMapping));
    }
}

TEST_CASE("TestRefCountedArgumentsMarshaling")
{
    {
        auto f = MakeFunction("f", MakeVoid(), {
            { "ptr", MakeRefCountedPointer("Context", false) },
            { "constPtr", MakeRefCountedPointer("Context", true) },
            { "ref", MakeRefCountedReference("Context", false) },
            { "constRef", MakeRefCountedReference("Context", true) },
        }, false);

        REQUIRE("void f(Object* self, Context* ptr, const Context* constPtr, Context* ref, const Context* constRef)"
            == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("self->f(ptr, constPtr, *ref, *constRef)"
            == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("void f(IntPtr self, IntPtr ptr, IntPtr constPtr, IntPtr ref, IntPtr constRef)"
            == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("void f(Context ptr, const Context constPtr, Context ref, const Context constRef)"
            == GenerateManagedFunctionDeclCSharp("Object", "f", f, emptyMapping));
        REQUIRE("f(Handle, ptr.Handle, constPtr.Handle, ref.Handle, constRef.Handle)"
            == GenerateManagedFunctionCallCSharp("Object", "f", f, emptyMapping));
    }
}

TEST_CASE("TestRefCountedReturnMarshaling")
{
    {
        auto f = MakeFunction("f", MakeRefCountedPointer("Context", false), {}, false);

        REQUIRE("Context* f(Object* self)" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("return self->f()" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("IntPtr f(IntPtr self)" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("Context f()" == GenerateManagedFunctionDeclCSharp("Object", "f", f, emptyMapping));
        REQUIRE("return Runtime.LookupObject<Context>(f(Handle))" == GenerateManagedFunctionCallCSharp("Object", "f", f, emptyMapping));
    }
    {
        auto f = MakeFunction("f", MakeRefCountedPointer("Context", true), {}, false);

        REQUIRE("const Context* f(Object* self)" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("return self->f()" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("IntPtr f(IntPtr self)" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("const Context f()" == GenerateManagedFunctionDeclCSharp("Object", "f", f, emptyMapping));
        REQUIRE("return Runtime.LookupObject<Context>(f(Handle))" == GenerateManagedFunctionCallCSharp("Object", "f", f, emptyMapping));
    }
    {
        auto f = MakeFunction("f", MakeRefCountedReference("Context", false), {}, false);

        REQUIRE("Context* f(Object* self)" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("return &self->f()" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("IntPtr f(IntPtr self)" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("Context f()" == GenerateManagedFunctionDeclCSharp("Object", "f", f, emptyMapping));
        REQUIRE("return Runtime.LookupObject<Context>(f(Handle))" == GenerateManagedFunctionCallCSharp("Object", "f", f, emptyMapping));
    }
    {
        auto f = MakeFunction("f", MakeRefCountedReference("Context", true), {}, false);

        REQUIRE("const Context* f(Object* self)" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("return &self->f()" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("IntPtr f(IntPtr self)" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("const Context f()" == GenerateManagedFunctionDeclCSharp("Object", "f", f, emptyMapping));
        REQUIRE("return Runtime.LookupObject<Context>(f(Handle))" == GenerateManagedFunctionCallCSharp("Object", "f", f, emptyMapping));
    }

}

TEST_CASE("TestObjectNameMapping")
{
    const StringMapping mapping = {
        { "Object", "ManagedObject" },
        { "FirstClass", "ManagedFirstClass" },
        { "SecondClass", "ManagedSecondClass" },
    };

    {
        auto f = MakeConstructor("f", "Object", "RefCounted", {
            { "first", MakeRefCountedPointer("FirstClass", false) },
            { "second", MakeRefCountedPointer("SecondClass", false) },
        });

        REQUIRE("RefCounted* f(FirstClass* first, SecondClass* second)" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("return new Object(first, second)" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("IntPtr f(IntPtr first, IntPtr second)" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("ManagedObject(ManagedFirstClass first, ManagedSecondClass second)" == GenerateManagedFunctionDeclCSharp("Object", "f", f, mapping));
        REQUIRE("f(first.Handle, second.Handle)" == GenerateManagedFunctionCallCSharp("Object", "f", f, mapping));
    }

    {
        auto f = MakeFunction("f", MakeRefCountedPointer("FirstClass", false), {
            { "second", MakeRefCountedPointer("SecondClass", false) },
        }, false);

        REQUIRE("FirstClass* f(Object* self, SecondClass* second)" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("return self->f(second)" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("IntPtr f(IntPtr self, IntPtr second)" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("ManagedFirstClass f(ManagedSecondClass second)" == GenerateManagedFunctionDeclCSharp("Object", "f", f, mapping));
        REQUIRE("return Runtime.LookupObject<ManagedFirstClass>(f(Handle, second.Handle))" == GenerateManagedFunctionCallCSharp("Object", "f", f, mapping));
    }
}

TEST_CASE("TestFunctionNameMapping")
{
    {
        auto f = MakeFunction("DoWork", MakeVoid(), {}, false);

        REQUIRE("void f(Object* self)" == GenerateCFunctionDeclNative("Object", "f", f));
        REQUIRE("self->DoWork()" == GenerateCxxFunctionCallNative("Object", "f", f));
        REQUIRE("void f(IntPtr self)" == GenerateCFunctionDeclCSharp("Object", "f", f));
        REQUIRE("void DoWork()" == GenerateManagedFunctionDeclCSharp("Object", "f", f, emptyMapping));
        REQUIRE("f(Handle)" == GenerateManagedFunctionCallCSharp("Object", "f", f, emptyMapping));
    }
}

// TEST_CASE("TestNativeToC_Primitive", "")
// {
//     REQUIRE("int" == GetTypeNameC(MakePrimitiveValue(PrimitiveTypeKind::Int)));
//     REQUIRE("char" == GetTypeNameC(MakePrimitiveValue(PrimitiveTypeKind::Bool)));
//     REQUIRE("double" == GetTypeNameC(MakePrimitiveValue(PrimitiveTypeKind::Double)));

//     REQUIRE("int*" == GetTypeNameC(MakePrimitivePointer(PrimitiveTypeKind::Int)));
//     REQUIRE("char*" == GetTypeNameC(MakePrimitivePointer(PrimitiveTypeKind::Bool)));
//     REQUIRE("double*" == GetTypeNameC(MakePrimitivePointer(PrimitiveTypeKind::Double)));
//
//     REQUIRE("int*" == GetTypeNameC(MakePrimitiveReference(PrimitiveTypeKind::Int)));
//     REQUIRE("char*" == GetTypeNameC(MakePrimitiveReference(PrimitiveTypeKind::Bool)));
//     REQUIRE("double*" == GetTypeNameC(MakePrimitiveReference(PrimitiveTypeKind::Double)));
//
//     REQUIRE("const int*" == GetTypeNameC(MakePrimitiveConstPointer(PrimitiveTypeKind::Int)));
//     REQUIRE("const char*" == GetTypeNameC(MakePrimitiveConstPointer(PrimitiveTypeKind::Bool)));
//     REQUIRE("const double*" == GetTypeNameC(MakePrimitiveConstPointer(PrimitiveTypeKind::Double)));
//
//     REQUIRE("const int*" == GetTypeNameC(MakePrimitiveConstReference(PrimitiveTypeKind::Int)));
//     REQUIRE("const char*" == GetTypeNameC(MakePrimitiveConstReference(PrimitiveTypeKind::Bool)));
//     REQUIRE("const double*" == GetTypeNameC(MakePrimitiveConstReference(PrimitiveTypeKind::Double)));
// }

// TEST_CASE("TestNativeToC_RefCounted", "")
// {
//     REQUIRE("Object" == GetTypeNameC(MakePrimitiveValue("Object")));
//
// }

}

}
