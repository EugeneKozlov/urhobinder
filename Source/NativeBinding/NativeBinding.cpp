#ifdef URHO3D_PLATFORM_WINDOWS
#define URHO3D_EXPORT_API __declspec(dllexport)
#else
#define URHO3D_EXPORT_API
#endif

#include <Urho3D/Core/ProcessUtils.h>

extern "C"
{

    URHO3D_EXPORT_API void binding_Engine_ParseArguments(char* args[], int nargs)
    {
        Urho3D::ParseArguments(nargs, args);
    }

}
