#ifdef URHO3D_PLATFORM_WINDOWS
#define URHO3D_EXPORT_API __declspec(dllexport)
#else
#define URHO3D_EXPORT_API
#endif

#include <Urho3D/Engine/Application.h>

using namespace Urho3D;

class Urho_Application_wrapper : public Application
{
public:
    using Callback = void(__stdcall *)(Application* self);
    Callback onSetup_ = 0;
    Callback onStart_ = 0;
    Callback onStop_ = 0;

    using Application::Application;
    virtual void Setup() { onSetup_(this); }
    virtual void Start() { onStart_(this); }
    virtual void Stop() { onStop_(this); }
};

extern "C"
{

URHO3D_EXPORT_API int ub_Urho_RefCounted_Refs(RefCounted* self)
{
    return self->Refs();
}

URHO3D_EXPORT_API RefCounted* ub_Urho_Context_Constructor()
{
    return new Context();
}

URHO3D_EXPORT_API RefCounted* ub_Urho_Application_Constructor(Context* context)
{
    return new Urho_Application_wrapper(context);
}

URHO3D_EXPORT_API int ub_Urho_Application_Run(Urho_Application_wrapper* self)
{
    return self->Run();
}

URHO3D_EXPORT_API void ub_Urho_Application_Setup_set(Urho_Application_wrapper* self, Urho_Application_wrapper::Callback callback)
{
    self->onSetup_ = callback;
}

URHO3D_EXPORT_API void ub_Urho_Application_Start_set(Urho_Application_wrapper* self, Urho_Application_wrapper::Callback callback)
{
    self->onStart_ = callback;
}

URHO3D_EXPORT_API void ub_Urho_Application_Stop_set(Urho_Application_wrapper* self, Urho_Application_wrapper::Callback callback)
{
    self->onStop_ = callback;
}

}
